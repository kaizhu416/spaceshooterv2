# Space Shooter  Reworked
### with Unity Engine

---

Based on the Unity Space Shooter tutorial project, modified to test custom spawning, webGL, and online leaderboard API.

### Primary goals of implementation
* Benchmark Unity WebGL performance
* Post and get operations with online leaderboard API
* Importing custom 2D and 3D assets
* Modular enemy, weapon, and bonus scripting

### To-dos:
* Implement android control scheme for mobile build
* New UI for level results
* Procedural obstacle generation
* Replacement of graphic and sound assets
